package ru.t1.kravtsov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.kravtsov.tm.api.service.IPropertyService;
import ru.t1.kravtsov.tm.api.service.IServiceLocator;
import ru.t1.kravtsov.tm.dto.request.ServerAboutRequest;
import ru.t1.kravtsov.tm.dto.request.ServerVersionRequest;
import ru.t1.kravtsov.tm.dto.response.ServerAboutResponse;
import ru.t1.kravtsov.tm.dto.response.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@WebService(endpointInterface = "ru.t1.kravtsov.tm.api.endpoint.ISystemEndpoint")
public class SystemEndpoint implements ISystemEndpoint {

    @NotNull
    private final IServiceLocator serviceLocator;

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @GET
    @NotNull
    @Override
    @WebMethod
    @Path("/getAbout")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRequest request
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @GET
    @NotNull
    @Override
    @WebMethod
    @Path("/getVersion")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRequest request
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}

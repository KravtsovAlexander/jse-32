package ru.t1.kravtsov.tm.component;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import lombok.Getter;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.t1.kravtsov.tm.api.component.IBootstrap;
import ru.t1.kravtsov.tm.api.endpoint.*;
import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.api.repository.ISessionRepository;
import ru.t1.kravtsov.tm.api.repository.ITaskRepository;
import ru.t1.kravtsov.tm.api.repository.IUserRepository;
import ru.t1.kravtsov.tm.api.service.*;
import ru.t1.kravtsov.tm.endpoint.*;
import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.model.Task;
import ru.t1.kravtsov.tm.model.User;
import ru.t1.kravtsov.tm.repository.ProjectRepository;
import ru.t1.kravtsov.tm.repository.SessionRepository;
import ru.t1.kravtsov.tm.repository.TaskRepository;
import ru.t1.kravtsov.tm.repository.UserRepository;
import ru.t1.kravtsov.tm.service.*;
import ru.t1.kravtsov.tm.util.SystemUtil;

import javax.ws.rs.Path;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;

public final class Bootstrap implements IBootstrap, IServiceLocator {

    @NotNull
    private final static String PACKAGE_COMMANDS = "ru.t1.kravtsov.tm.command";

    @NotNull
    private final static Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    @NotNull
    private final static Logger LOGGER_COMMANDS = LoggerFactory.getLogger("COMMANDS");

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final JacksonJaxbJsonProvider jsonProvider = new JacksonJaxbJsonProvider();

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = "0.0.0.0";
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        {
            @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
            @NotNull final JaxWsServerFactoryBean serverFactory = new JaxWsServerFactoryBean();
            serverFactory.setAddress(url);
            serverFactory.setServiceBean(endpoint);
            serverFactory.create();
            System.out.println(url);
        }
        {
            if (!endpoint.getClass().isAnnotationPresent(Path.class)) return;
            @NotNull final String baseUrl = "http://" + host + ":" + port + "/";
            @NotNull final JAXRSServerFactoryBean bean = new JAXRSServerFactoryBean();
            bean.setProviders(Collections.singletonList(jsonProvider));
            bean.setResourceClasses(endpoint.getClass());
            bean.setResourceProvider(endpoint.getClass(), new SingletonResourceProvider(endpoint));
            bean.setAddress(baseUrl);
            bean.create();
        }
    }

    @Override
    public void run(@Nullable String[] args) {
        initPID();
        initDemoData();
        LOGGER_LIFECYCLE.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
    }

    private void prepareShutdown() {
        backup.stop();
        LOGGER_LIFECYCLE.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        userService.create("test", "test", "test@test.ru");
        @NotNull final User user = userService.create("user", "user", "user@user.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(user.getId(), new Project("BETA PROJECT", Status.NOT_STARTED));
        projectService.add(user.getId(), new Project("ALPHA PROJECT", Status.IN_PROGRESS));
        projectService.add(admin.getId(), new Project("GAMMA PROJECT", Status.COMPLETED));

        taskService.add(user.getId(), new Task("DELTA TASK"));
        taskService.add(admin.getId(), new Task("EPSILON TASK"));
    }

}

package ru.t1.kravtsov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.model.Task;

@NoArgsConstructor
public class TaskStartByIndexResponse extends AbstractTaskResponse {

    public TaskStartByIndexResponse(final @Nullable Task task) {
        super(task);
    }

}

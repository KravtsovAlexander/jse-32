package ru.t1.kravtsov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class TaskRemoveByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public TaskRemoveByIndexRequest(final @Nullable String token) {
        super(token);
    }

}

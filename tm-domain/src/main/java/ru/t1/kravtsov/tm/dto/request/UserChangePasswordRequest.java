package ru.t1.kravtsov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class UserChangePasswordRequest extends AbstractUserRequest {

    @Nullable
    private String password;

    public UserChangePasswordRequest(final @Nullable String token) {
        super(token);
    }

}

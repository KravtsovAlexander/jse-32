package ru.t1.kravtsov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class TaskStartByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskStartByIdRequest(final @Nullable String token) {
        super(token);
    }

}

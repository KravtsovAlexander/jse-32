package ru.t1.kravtsov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class UserViewProfileRequest extends AbstractUserRequest {

    public UserViewProfileRequest(final @Nullable String token) {
        super(token);
    }

}

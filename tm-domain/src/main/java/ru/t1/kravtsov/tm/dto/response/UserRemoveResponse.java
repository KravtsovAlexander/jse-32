package ru.t1.kravtsov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.model.User;

@NoArgsConstructor
public class UserRemoveResponse extends AbstractUserResponse {

    public UserRemoveResponse(final @Nullable User user) {
        super(user);
    }

}

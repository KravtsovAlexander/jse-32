package ru.t1.kravtsov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.dto.request.*;
import ru.t1.kravtsov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@WebService
@Path("/api/DomainEndpoint")
public interface IDomainEndpoint extends IEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @POST
    @NotNull
    @WebMethod
    @Path("/loadDataBackup")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    DataBackupLoadResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupLoadRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/saveDataBackup")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    DataBackupSaveResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupSaveRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/loadDataBase64")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    DataBase64LoadResponse loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64LoadRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/saveDataBase64")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    DataBase64SaveResponse saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64SaveRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/loadDataBinary")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    DataBinaryLoadResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinaryLoadRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/saveDataBinary")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    DataBinarySaveResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinarySaveRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/loadDataJsonFasterXml")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonLoadFasterXmlRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/saveDataJsonFasterXml")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonSaveFasterXmlRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/loadDataJsonJaxB")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    DataJsonLoadJaxBResponse loadDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonLoadJaxBRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/saveDataJsonJaxB")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    DataJsonSaveJaxBResponse saveDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonSaveJaxBRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/loadDataXmlFasterXml")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlLoadFasterXmlRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/saveDataXmlFasterXml")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlSaveFasterXmlRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/loadDataXmlJaxB")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    DataXmlLoadJaxBResponse loadDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlLoadJaxBRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/saveDataXmlJaxB")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    DataXmlSaveJaxBResponse saveDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlSaveJaxBRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/loadDataYamlFasterXml")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlLoadFasterXmlRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/saveDataYamlFasterXml")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlSaveFasterXmlRequest request
    );

}

package ru.t1.kravtsov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public class ProjectClearRequest extends AbstractUserRequest {

    public ProjectClearRequest(final @Nullable String token) {
        super(token);
    }

}

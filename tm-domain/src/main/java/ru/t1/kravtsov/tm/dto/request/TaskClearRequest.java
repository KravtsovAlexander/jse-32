package ru.t1.kravtsov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public class TaskClearRequest extends AbstractUserRequest {

    public TaskClearRequest(final @Nullable String token) {
        super(token);
    }

}

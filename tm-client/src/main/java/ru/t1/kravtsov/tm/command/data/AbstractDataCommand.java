package ru.t1.kravtsov.tm.command.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.kravtsov.tm.command.AbstractCommand;

@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    IDomainEndpoint getDomainEndpoint() {
        return getServiceLocator().getDomainEndpoint();
    }

}
